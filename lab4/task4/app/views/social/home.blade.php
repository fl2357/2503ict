@extends('layouts.master')

@section('array')
  <?php
  $posts = array(
      array('date' =>'1/1/15', 'message' => 'message 1', 'image' => 'images/hide.jpg'),
      array('date' =>'2/1/15', 'message' => 'message 2', 'image' => 'images/hide.jpg'),
      array('date' =>'3/1/15', 'message' => 'message 3', 'image' => 'images/hide.jpg'),
      array('date' =>'4/1/15', 'message' => 'message 4', 'image' => 'images/hide.jpg'),
      array('date' =>'5/1/15', 'message' => 'message 5', 'image' => 'images/hide.jpg'),
      array('date' =>'6/1/15', 'message' => 'message 6', 'image' => 'images/hide.jpg'),
      array('date' =>'7/1/15', 'message' => 'message 7', 'image' => 'images/hide.jpg'),
      array('date' =>'8/1/15', 'message' => 'message 8', 'image' => 'images/hide.jpg'),
      array('date' =>'9/1/15', 'message' => 'message 9', 'image' => 'images/hide.jpg'),
      array('date' =>'10/1/15', 'message' => 'message 10', 'image' => 'images/hide.jpg'),
    );
?>
@show

@section('post')
             <form>
               Name: <br>
               <input type='text' name='name'>
               <br>
             </form>
             <form> 
               New Message: <br>
               <textarea rows='4' cols='10'>Enter Your Message</textarea> <br>
               <button type="button" class="btn btn-primary btn-md">Submit</button>
             </form>
@stop

@section('content')
            <h2>  Heath McCutcheon-Fletcher</h2>
            <?php
              $num =rand(1, 10);
              echo "<br>$num<br>";
             for ($i=0; $i <$num; $i++){
               $image = $posts[$i]['image'];
               $date = $posts[$i]['date'];
               $message = $posts[$i]['message'];
               echo "<div class='post'>";
               echo "<img class= 'photo' src='$image' alt='photo'>";
               echo "Date: $date <br>";
               echo "$message <br>";
               echo "</div>";
             }
             ?>
@stop