@section('array')

@show

<!DOCTYPE html>
<!-- Checked with w3C found 2 errors on unclosed Div and Body think might be because of home.blade.php --!>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Lab2 Social</title>
        <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">

        <!-- Bootstrap core JavaScript
    ================================================== -->
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  
  </head>

     <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">s2724222 Social</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="./">Photos </a></li>
              <li><a href="../navbar-static-top/">Friends</a></li>
              <li><a href="../navbar-fixed-top/">Logon</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main body -->
     <div class='row'>
         <div class='col-xs-4'>
             @section('post')
             <form>
               Name: <br>
               <input type='text' name='name'>
               <br>
             </form>
             <form> 
               New Message: <br>
               <textarea rows='4' cols='10'>Enter Your Message</textarea> <br>
               <button type="button" class="btn btn-primary btn-md">Submit</button>
             </form>
             @show
         </div>
         <div class='col-xs-8'>
            @section('content')
         </div>
          @show
     </div>

    </div> <!-- /container -->


  </body>
</html>