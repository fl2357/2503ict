<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for $name or $year or $state from form. */
function search($name) {
    global $pms; 

// Filter $pms by $state
    
    if (!empty($name)) {
        $results = array();
        foreach ($pms as $pm) {
            if (stripos($pm['state'], $name) !== FALSE) {
                $results[] = $pm;
            }
            elseif (stripos($pm['name'], $name) !== FALSE) {
                $results[] = $pm;
            }
            elseif (strpos($pm['from'], $name) !== FALSE || 
                strpos($pm['to'], $name) !== FALSE) {
                $results[] = $pm;
            }
        }
        $pms = $results;
    }

 /*   // Filter $pms by $name
    if (!empty($name)) {
        $results = array();
        foreach ($pms as $pm) {
            if (stripos($pm['name'], $name) !== FALSE) {
                $results[] = $pm;
            }
        }
        $pms = $results;
    }

    // Filter $pms by $year
    if (!empty($year)) {
        $results = array();
        foreach ($pms as $pm) {
            if (strpos($pm['from'], $year) !== FALSE || 
                strpos($pm['to'], $year) !== FALSE) {
                $results[] = $pm;
            }
        }
        $pms = $results;
    }

    // Filter $pms by $state
    if (!empty($name)) {
        $results = array();
        foreach ($pms as $pm) {
            if (stripos($pm['state'], $name) !== FALSE) {
                $results[] = $pm;
            }
        }
        $pms = $results;
    }
 
 */
    return $pms;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta name="generator" content="HTML Tidy for Linux (vers 25 March 2009), see www.w3.org">
<title></title>
</head>
<body>
</body>
</html>