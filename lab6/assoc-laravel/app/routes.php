<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/pms.php";


// Display search form
Route::get('/', function()
{
	return View::make('pms.query');
});

// Perform search and display results
Route::get('search', function()
{
  $name = Input::get('name');
  /* $year = Input::get('year');
  $state = Input::get('state'); */

  $results = search($name);

	return View::make('pms.results')->withPms($results);
});


/* Functions for PM database example. 

  $sql = "select * from pms";
  $result = DB::select($sql);
  print_r($result);
  exit; */

/* Search sample data for $name or $year or $state from form. */
function search($name) {
  // Filter $pms by $name
  if (!empty($name)) {
    $sql = "select * from pms where name like ?";
    $pms = DB::select($sql, array("%$name%"));
    /* $queries = DB::getQueryLog();
    print_r($queries);
    print_r($pms);
    exit; */
    }
    return $pms;
   /* 
   
       foreach ($pms as $pm) {
      if (stripos($pm['name'], $name) !== FALSE) {
        $results[] = $pm;
      elseif (stripos($pm['phone'], $name) !== FALSE) {
            $results[] = $pm;
           }
      elseif (stripos($pm['address'], $name) !== FALSE) {
            $results[] = $pm;
           }
      elseif (stripos($pm['email'], $name) !== FALSE) {
            $results[] = $pm;
           }
        }
      $pms = $results;
    }
/*
  // Filter $pms by $year
  if (!empty($year)) {
    $results = array();
    foreach ($pms as $pm) {
      if (strpos($pm['from'], $year) !== FALSE || 
          strpos($pm['to'], $year) !== FALSE) {
        $results[] = $pm;
      }
    }
    $pms = $results;
  }

  // Filter $pms by $state
  if (!empty($state)) {
    $results = array();
    foreach ($pms as $pm) {
      if (stripos($pm['state'], $state) !== FALSE) {
        $results[] = $pm;
      }
    }
    $pms = $results;
  }

  return $pms;
  */
}
