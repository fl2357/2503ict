<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */



// Load the homepage with current posts
Route::get('/', function()
{
    $sql = "select * from posts ORDER BY ID DESC";
    $current_posts = DB::select($sql);
  
    $results=$current_posts;
    
	return View::make('pages.home')->withposts($results);
});


// Add a new post and then reload the home page, shows new post (I can't believe this took two whole days I must be *slow*)
Route::post('add_post_action', function()
{
  $post_title = Input::get('new_post_title');
  $post_username= Input::get('new_post_username');
  $post_message = Input::get('new_post_message');
  
  $id = add_post($post_title, $post_username, $post_message);
  
    $sql = "select * from posts ORDER BY ID DESC";
    $current_posts = DB::select($sql);
  
    $results=$current_posts;
    
	return View::make('pages.home')->withposts($results);
});
// function take form data and turn them into variables then inserting those variables into the sql
function add_post($post_title, $post_username, $post_message)
  {
    $sql = "insert into posts (post_username, post_title, post_message) values (?, ?, ?)";
    DB::insert($sql, array($post_username, $post_title, $post_message));
    $id = DB::getPdo()->lastInsertId();
    return $id;
}


/* Function To Make an Update Page  get's post ID from a button generated in the @foreach loop*/
 Route::post('update_page', function()
{
    $id = Input::get('update_id');
    $sql = "select * from posts where id = ?";
    $current_posts = DB::select($sql, array($id));
  
    $results=$current_posts;
    
	  return View::make('pages.update')->withposts($results);

});


/* Delete A post */


Route::post('delete_post_action', function()
{   /* Get post->id from hidden input then delete row  */       
    $id = Input::get('delete_id');
    $sql = "delete from posts where id = ?";
    DB::delete($sql, array($id));
  
    /* Rebuild webpage */ 
    $sql = "select * from posts ORDER BY ID DESC";
    $current_posts = DB::select($sql);
    $results=$current_posts;
    
	  return View::make('pages.home')->withposts($results);
});

Route::post('update_post_action', function()
{
    /* pass new form data into database */
    $new_post_message = Input::get('new_post_message');
    $id = Input::get('new_update_id');
    $sql = "update posts set post_message = ? where id = ?";
    DB::update($sql, array($new_post_message, $id));  
  
  
    /* rebuild new home again */
    $sql = "select * from posts";
    $current_posts = DB::select($sql);
  
    $results=$current_posts;
    
	return View::make('pages.home')->withposts($results);
});


Route::post('view_comments_page', function()
{
    /* Info for comments section */
    $c_id = Input::get('comments_id');
    $c_sql = "select * from comments where p_id = ?";
    $current_comments = DB::select($c_sql, array($c_id));
    
    /* Info to display Post */
    $id = Input::get('comments_id');
    $sql= "select * from posts where id = ?";
    $current_post  = DB::select($sql, array($id));
  
    $c_results=$current_comments;
    $results=$current_post;
    
	return View::make('pages.comments')->withcomments($c_results)->withposts($results);
});

/* Delete Comments Button */
Route::post('delete_comment_action', function()
{
    /* Store information about the post so that it can be re-built */
    $p_id = Input::get('delete_p_id');        
    $sql = "select * from posts where id = ?";
    $current_post = DB::select($sql, array($p_id));  
    $results = $current_post;
    /* delete the unwanted comment */
    $delete_c_id = Input::get('delete_c_id');
    $delete_c_sql = "delete from comments where c_id = ?";
    DB::delete($delete_c_sql, array($delete_c_id));
    
    /* store other current comments */
    $c_id = Input::get('delete_p_id');
    $c_sql = "select * from comments where p_id = ?";
    $current_comments = DB::select($c_sql, array($c_id));
  

    $c_results=$current_comments;
    $results=$current_post;
    
  	return View::make('pages.comments')->withcomments($c_results)->withposts($results);
});
    
Route::post('add_comment_action', function()
{
  $comment_username= Input::get('new_comment_username');
  $comment_message = Input::get('new_comment_message');
  $new_p_id = Input::get('new_comment_p_id');
  
  $c_new_id = add_comment($comment_username, $comment_message, $new_p_id);
  
    $p_id = Input::get('new_comment_p_id');        
    $sql = "select * from posts where id = ?";
    $current_post = DB::select($sql, array($p_id));  
    
    
    $c_id = Input::get('new_comment_p_id');
    $c_sql = "select * from comments where p_id = ?";
    $current_comments = DB::select($c_sql, array($c_id));
    
    $c_results=$current_comments;
    $results=$current_post;
    
  	return View::make('pages.comments')->withcomments($c_results)->withposts($results);
});

function add_comment($comment_username, $comment_message, $new_p_id)
  {
    $insert_c_sql = "insert into comments (comment_username, comment_message, p_id) values (?, ?, ?)";
    DB::insert($insert_c_sql, array($comment_username, $comment_message, $new_p_id));
    $c_new_id = DB::getPdo()->lastInsertId();
    return $c_new_id;
}
/*  NOTE:  Below here is a whole metric ton of FAIL Proceed at own risk */

/*
function update_post($id, $new_post_message)
{
    $new_post_message = Input::get('new_post_message');
    $id = Input::get('new_update_id');
    $sql = "update posts set post_message = updated, where id = 4";
    DB::update($sql, array($new_post_message, $id));
}

/*
Route::post('add_post_action', function()
{
    $post_title = Input::get('new_post_title');
    $post_username= Input::get('new_post_username');
    $post = Input::get('new_post_message');  
});

/*
Route::get('posts', function()
{
  $sql = 'select * from posts';
  $results = DB::select($sql);
  $post = $results[0];
  $post_username = $post->post_username;
  $post_title = $post->post_title;
  $post_message = $post->post;
  return "post_username: $post_username, post_title: $post_title, message: $message";
});
/*
Route::get('posts_list', function()
{
  $results = DB::select('select * from posts');
  return View::make('posts_list')->withposts($results);
});
/*

Route::get('search', function()
{
  $post_title = Input::get('post_title');
  $post_username= Input::get('post_username');
  $post_message = Input::get('post');  

  $results = search($post_title);
  
  return View::make('pages.comments')->withposts($results);
});

function search($post_username) {
  if (!empty($post_username)) {  
    $sql = "select * from posts where post_title like ?";
    $results = DB::select($sql, array("%$post_username%"));
  }
  return $results;
}
*/ 


  
  
/* initial attempt at delete posts
function delete_item($id)
{
  $id = Value::get('delete_id');
  $sql = "delete from posts where id = ?";
  DB::delete($sql, array($id));
}


/* Functions for PM database example. */
 

 
  /*function add_post($post_username, $post_title, $post) {
  $sql = "insert into posts (post_username, post_title, post) values (?, ?, ?)";
  DB::insert($sql, array($post_username, $post_title, $post));
  }
  */



/*function search($post_username) {

  if (!empty($post_username)) {  
    $sql = "select * from posts where post_title like ?";
    $posts = DB::select($sql, array("%$post_username%"));
  }
  return $posts;
}
/*
