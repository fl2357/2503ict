/* Posts database in SQLite. */
drop table if exists posts;

create table posts (
  id integer primary key autoincrement,
  post_username varchar(40) not null,
  post_title varchar(40),
  post_message varchar(140)
);

/* Column names changed to avoid SQLite reserved words. */

insert into posts(post_username, post_title, post_message) values ('first poster', 'first title', 'first');
insert into posts(post_username, post_title, post_message) values ('second poster', 'second title', 'second post');
insert into posts(post_username, post_title, post_message) values ('Test', 'Test', 'Test');
/* Sample query to test insertion worked. */

select * from posts
where post_username like "%second%"
order by id;

drop table if exists comments;

create table comments (
  c_id integer primary key autoincrement,
  comment_username varchar(40) not null,
  comment_message varchar(140),
  p_id int,
  FOREIGN KEY(p_id) REFERENCES posts(id)
);

/* Column names changed to avoid SQLite reserved words. */

insert into comments(comment_username, comment_message, p_id) values ('first commenter', 'first comment on first post', 1);
insert into comments(comment_username, comment_message, p_id) values ('second commenter', 'second comment on first post', 1);
insert into comments(comment_username, comment_message, p_id) values ('third commenter', 'first comment on second post', 2);
/* Sample query to test insertion worked. */

select * from comments
where comment_username like "%first%"
order by c_id;