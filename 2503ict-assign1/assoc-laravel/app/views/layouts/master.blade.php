<!-- display results -->
<!DOCTYPE html>
<!-- Results page of associative array search example. -->
<html>
<head>
    @include('includes.head')
    <title>@yield('title')</title>
</head>

<body>
  @include('includes.header') 
  @yield('content')

  @include('includes.footer') 
</body>
</html>