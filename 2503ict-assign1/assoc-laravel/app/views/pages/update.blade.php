@extends('layouts.master')

@section('title')
This is the Update pages
@stop

@section('content')
        <h2>Update Your Post</h2>
        
        
@foreach ($posts as $post)
    <form method="post" action="{{{ url('update_post_action') }}}" class="form-inline">
        <input type="hidden" name="new_update_id" value="{{{ $post->id }}}">
        <table>
            <tr><td><label for="new_post_message">Enter Your Message</label><input type="text" name="new_post_message"value="{{{ $post->post_message }}}" required=""></td></tr>
            <tr><td colspan=2><input type="submit" class="btn btn-primary" value="Update item"></td></tr>
            <tr><input type="reset" class="btn btn-warning" value="Reset"></td></tr>
        </table>
    </form>
@endforeach
@stop
<!--
@foreach ($posts as $post)
     <form method="post" action="{{{ url('update_post_action') }}}">
            <input type="hidden" name="id" value="{{{ $post->id }}}">
            <table>
            <tr><td>Post Message</td> <td><input type="text" name="post_message"value="{{{ $post->post_message }}}"></td></tr>
            <tr><td colspan=2><input type="submit" value="Update item"></td></tr>
            </table>
            </form>
@endforeach
-->
@stop
