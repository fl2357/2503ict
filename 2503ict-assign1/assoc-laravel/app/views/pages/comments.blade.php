@extends('layouts.master')

@section('title')
The Comments Page
@stop

@section('content')

    <h2>Comments Page</h2>
    <h3> Current Post:</h3>
@foreach($posts as $post)
        <table class="table">
          <thead>
            <tr>
            <th class="col-sm-1">No.</th>
            <th class="col-sm-1">Post Username</th>
            <th class="col-sm-1">Post Title</th>
            <th class="col-sm-1">Post</th></tr>
          </thead>
          <tbody>
              <tr class="info">
                  <td>{{{ $post->id }}}</td>
                  <td>{{{ $post->post_username }}}</td>
                  <td>{{{ $post->post_title }}}</td>
                  <td>{{{ $post->post_message }}}</td>
              </tr>
          </tbody>
        </table>


    <h3>This Posts's Comments</h3>

  <form method="post" action="{{{ url('add_comment_action') }}}" class="form-inline">
  <table>
    <tr><td><label for="new_comment_username">Your Username</label></td><td><input type="text" name="new_comment_username" required=""  placeholder="Enter Your Username"></td></tr>
    <tr><td><label for="new_comment_message">Your Comment</label> </td><td><textarea name="new_comment_message" required=""  placeholder="Song Name?"></textarea></td></tr>
    <input type="hidden" name="new_comment_p_id" value="{{{ $post->id }}}">
    <tr><td colspan=2><input type="submit" class="btn btn-primary" value="Add Comment"></td></tr>
  </table>
  </form> 
@endforeach

@if (count($comments) == 0)

<p>No Comments found.</p>

@else 
    <table class="table">
        <thead>
            <tr>
                <th class="col-sm-1">ID</th>
                <th class="col-sm-1">comment_message</th>
                <th class="col-sm-1">p_id</th>
                <th class="col-sm-1">Delete This Comment</th>
            </tr>
        </thead>
        <tbody>

@foreach($comments as $comment)
            <tr>
                <td>{{{ $comment->c_id }}}</td>
                <td>{{{ $comment->comment_message }}}</td>
                <td>{{{ $comment->p_id }}}</td>
                <td>    
                    <form method="post" action="{{{ url('delete_comment_action') }}}">
                    <input type="hidden" name="delete_p_id" value="{{{ $comment->p_id }}}">
                    <input type="hidden" name="delete_c_id" value="{{{ $comment->c_id }}}">
                    <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </td>
            </tr>
@endforeach

        </tbody>
    </table>
@endif
@stop