@extends('layouts.master')

@section('title')
Display posts databa
@stop

@section('content')





  <!-- Table to add  a new post into posts.sql -->

        <h2>Submit a New post</h2>
        <h3>All Fields Required</h3>
  <form method="post" action="{{{ url('add_post_action') }}}" class="form-inline">>
    <div class="form-group">
  <table>
    
    <tr><td><label for="new_post_title">Post Title</label></td> <td><input type="text" name="new_post_title" required=""  placeholder="Enter a title"></td></tr>
    <tr><td><label for="new_post_username">Your Username:</label></td><td><input type="text" name="new_post_username" required="" placeholder="Enter Your username"></td></tr>
    <tr><td><label for="new_post_message">Enter Your Message</label></td> 
      <div class="col-md-10">
         <td><textarea class="form-control" rows="3" name="new_post_message" required="" placeholder="Personal Info Please.. I promise not to sell your info"></textarea></td></tr>
      </div>
    <tr>
        <td><input type="submit" class="btn btn-primary" value="Add post"></td>
        <td><input type="reset" class="btn btn-warning" value="Reset"></td>
    </tr>
  </table>
    </div>
  </form>     
  <!-- View of current Posts in posts. -->     


@if (count($posts) == 0)
<p>No results found.</p>

@else
    <table class="table">
      <thead>
        <tr>
          <th class="col-sm-1">ID No.</th>
          <th class="col-sm-1">Post Username</th>
          <th class="col-sm-1">Post Title</th>
          <th class="col-sm-2">Post</th>
          <th class="col-sm-1">Delete This post</th>
          <th class="col-sm-1">Update This Post</th>
          <th class="col-sm-1">View Comments</th>
          <th class="col-sm-2">Image</th>
        </tr>
      </thead>
      <tbody>
@foreach ($posts as $post)
        <tr>
          <td>{{{ $post->id }}}</td>
          <td>{{{ $post->post_username }}}</td>
          <td>{{{ $post->post_title }}}</td>
          <td>{{{ $post->post_message }}}</td>
          <td> <!-- Creates a New #Swaggy Button that links to routes to delete item --> 
            <form method="post" action="{{{ url('delete_post_action') }}}">
            <input type="hidden" name="delete_id" value="{{{ $post->id }}}">
            <input type="submit" class="btn btn-danger" value="Delete">
            </form>
          </td>
          <td> <!-- Button to create a new page  --> 
            <form method="post" action="{{{ url('update_page') }}}">
            <input type="hidden" name="update_id" value="{{{ $post->id }}}">
            <input type="submit" class="btn btn-warning" value="Update">
            </form>
          </td>
          <td>
            <form method="post" action="{{{ url('view_comments_page') }}}">
            <input type="hidden" name="comments_id" value="{{{ $post->id }}}">
            <input type="submit" class="btn btn-primary" value="View Comment">
            </form>
          </td>
          <td>
            <img src="vaultboy.png" alt="#money" width="64" height="64">
          </td>
        </tr>
@endforeach
    </tbody>
  </table>
@endif
@stop






<!-- Attempt at update tables within home
@if (count($posts) == 0)

<p>No results found.</p>

@else   
  <form method="old_posts" action="{{{ url('update_post_action') }}}">
  <table>
@foreach ($posts as $post)
        <tr><td><input type="text" name="id" value="{{{ $post->id }}}"></td><td>Post Username</td><td><input type="text" name="post_message"value="{{{ $post->post_message }}}"></td><td>{{{ $post->post_username }}}</td><td>{{{ $post->post_title }}}</td><td><input type="submit" value="Delete"></td><td><input type="submit" value="Update"></td></tr>
@endforeach

    </tbody>
  </table>
@endif
@stop
-->   
 <!--         DRAFT TABLE for visual idea  
  <table>
  </form>
  <h3>Previous Posts</h3>
  <table class="bordered">
    <thead>
      <th>Number</th><th>Username</th><th>Title</th><th>Post</th> <th>View Comments</th><th>Delete</th> 
    </thead>
    <tbody>
      <tr><td>The ID NO</td><td>The Posts Username</td><td>The Posts Title</td><td>The Post</td><td><input type="submit" value="view"></td><td><input type="submit" value="delete"></td></tr>
   </tbody>
  </table>

@stop

  <!-- View of current Posts -->  
<!-- Saved Safe Table to display database data 
@if (count($posts) == 0)

<p>No results found.</p>

@else   
    <table class="bordered">
      <thead>
        <tr><th>No.</th><th>Post Username</th><th>Post Title</th><th>Post</th><th>Delete This post</th><td>Update This Post</td></tr>
      </thead>
      <tbody>
@foreach ($posts as $post)
        <tr><td>{{{ $post->id }}}</td><td>{{{ $post->post_username }}}</td><td>{{{ $post->post_title }}}</td><td>{{{ $post->post_message }}}</td><td><input type="submit" value="Delete"></td><td><input type="submit" value="Update"></td></tr>
@endforeach

    </tbody>
  </table>
@endif
@stop

-->

<!-- Initial Table that I modified from lab 6 to confirm that information could be 
      taken from the databse posts.sql
  <form method="get" action="search">
  <table>
    <tr><td>post_title: </td><td><input type="text" name="post_title"></td></tr>
    <tr><td>post_username: </td><td><input type="text" name="post_username"></td></tr>
    <tr><td>post: </td><td><input type="textarea" name="post"></td></tr>
    <tr><td colspan=2><input type="submit" value="search">
                      <input type="reset" value="Reset"></td></tr>
 
-->