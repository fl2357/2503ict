/* Posts database in SQLite. */
drop table if exists posts;

create table posts (
  id integer primary key autoincrement,
  number int, /* 0 = subsequent term */
  post_username varchar(40) not null,
  post_title varchar(40),
  post_message varchar(140)
);

/* Column names changed to avoid SQLite reserved words. */

insert into posts(post_username, post_title, post_message) values ('first poster', 'first title', 'first');
insert into posts(post_username, post_title, post_message) values ('second poster', 'second title', 'second post');
insert into posts(post_username, post_title, post_message) values ('Test', 'Test', 'Test');
/* Sample query to test insertion worked. */

select * from posts
where post_username like "%second%"
order by id;

drop table if exists comments;

create table comments (
  id integer primary key autoincrement,
  number int, /* 0 = subsequent term */
  comment_username varchar(40) not null,
  comment_message varchar(140)
);

/* Column names changed to avoid SQLite reserved words. */

insert into comments(comment_username, comment_message) values ('first commenter', 'first comment');
insert into comments(comment_username, comment_message) values ('second commenter', 'second comment');
insert into comments(comment_username, comment_message) values ('Test', 'Test');
/* Sample query to test insertion worked. */

select * from comments
where comment_username like "%first%"
order by id;